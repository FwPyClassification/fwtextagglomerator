import operator, re, sys
from collections import defaultdict
from FwText import (
    word_tokenize, split_on_edge_punctuation,
    ngram_table, get_phrase_position,
    remove_all_punctuation, word_instr
)
from FwFreq import freq, freq_dict_to_percent

class TextAgglomerator(object):
    ''' tokenize the texts with longest ngrams across the texts
        analyze before/after frequencies
        in order of length, insert each token into the new synth text
        keep duplicate ngrams to max of # in single text
        list which ngrams don't appear in all texts '''
    def __init__(self, a_texts):
        self.texts = [str(s_text) for s_text in a_texts if str(s_text) != 'nan']
        self.text_count = len(self.texts)
        
        if self.texts and isinstance(self.texts[0], (list,tuple)):
            self.text_words = self.texts #if already pre-tokenized
        elif self.texts and isinstance(self.texts[0], str):
            self.text_words = [split_on_edge_punctuation(word_tokenize(s_text, stop_words=None, remove_punct=False))
                               for s_text in self.texts]
        
        self.lengths = [len(a_text) for a_text in self.text_words]
        
        # max ngram size needs to be equal to shortest text to avoid overlapping common ngrams
        self.shortest = min(self.lengths)
        
        # used to calculated largest possible uncommon token size
        self.longest = max(self.lengths)
        
        self.agglomerate()
        
    
    @property
    def combined(self):
        s_new = ' '.join(self.synth)
        return re.sub(r" (?=[;:,.!?])", "", s_new)
    
    @property
    def varying(self):
        s_varying = ' | '.join([' '.join(t_ngram[1]) for t_ngram in self.uncommon_tokens])
        return re.sub(r" (?=[;:,.!?])", "", s_varying)
    
    
    def agglomerate(self):
        #list of ngrams with id,ngram,frequency,coverage,max,length
        self.ngrams = sorted(ngram_table(self.text_words,
                                         i_max_size=self.shortest,
                                         remove_punctuation=False,
                                         b_case_insensitive=True,
                                         b_use_tuples=True),
                             key=operator.itemgetter(5), reverse=True)
        
        #get the non-overlapping common and uncommon ngrams to use for tokenizing
        self.get_common_tokens()
        self.get_uncommon_tokens()
        
        self.text_tokens = []
        for a_text in self.text_words[:]:
            self.tokenize_ngrams(a_text, self.common_tokens)
            self.tokenize_ngrams(a_text, self.uncommon_tokens)
            self.text_tokens.append(a_text)
        
        self.get_relative_freqs()
        self.synthesize_pattern()
        return self
        
    
    def get_common_tokens(self):
        ''' Create a list of the common, non-duplicate tokens within the ngram list. '''
        at_common = [t_ngram for t_ngram in self.ngrams if t_ngram[3] == self.text_count]
        
        self.common_str, self.common_tokens, self.common_word_count = '', [], 0
        for t_ngram in at_common:
            s_ngram = ' '.join(t_ngram[1])
            s_ngram_lower = s_ngram.lower()
            if not word_instr(self.common_str, s_ngram_lower):
                self.common_str += '||{}'.format(s_ngram_lower)
                self.common_tokens.append(t_ngram)
                self.common_word_count += t_ngram[5]
        
    
    def get_uncommon_tokens(self):
        ''' Create a list of the non-common, non-duplicate tokens within the ngram list. '''   
        at_uncommon = [t_ngram for t_ngram in self.ngrams
                       if t_ngram[3] < self.text_count and
                       t_ngram[5] <= self.longest - self.common_word_count]
        at_possible_uncommon_tokens = []
        for t_ngram in at_uncommon:
            #loop through each word in the ngram and check for it's presence in other common
            b_overlapping_ngram = False
            for s_word in t_ngram[1]:
                if word_instr(self.common_str, s_word.lower()):
                    b_overlapping_ngram = True
                    break
            if not b_overlapping_ngram:
                at_possible_uncommon_tokens.append(t_ngram)

        e_overlap = {}
        for index,t_ngram in enumerate(at_possible_uncommon_tokens):  # loop through each possible uncommon
            for t_uc in at_possible_uncommon_tokens[:index]:  # loop through the previously checked uncommon
                if t_uc[1] in e_overlap: continue  # skip already invalid tokens
                i_matching = 0
                for s_word in t_ngram[1]:  # check each word to see if it appears in the previously checked uncommon
                    if s_word in t_uc[1]:
                        i_matching += 1

                if i_matching == t_ngram[5]:  # if current token is completely contained in previous, then current ngram is invalid
                    e_overlap[t_ngram[1]] = True
                elif i_matching > 0:  # if current token is partially contained in previous, then current previous is invalid
                    e_overlap[t_uc[1]] = True

        # filter out tokens that failed checks
        self.uncommon_tokens = [t_ngram for t_ngram in at_possible_uncommon_tokens
                                if not t_ngram[1] in e_overlap]
        
    
    def tokenize_ngrams(self, a_tokens, a_ngram_table):
        ''' Combine the ngrams in the tokenized sentence together '''
        for a_phrase in a_ngram_table:
            t_pos = get_phrase_position(a_phrase[1], a_tokens, b_case_insensitive=True, b_punctuation_insensitive=True)
            if t_pos: #if ngram phrase was found in sentence
                a_tokens[t_pos[0]:t_pos[1]] = [' '.join(a_phrase[1])]
        
    
    def get_relative_freqs(self):
        ''' Get the frequencies for each token of the other tokens they appear before and after.
            Return a dictionary of sorted lists of most frequent relative positions in descending order. '''
        ea_before,ea_after = defaultdict(list),defaultdict(list)
        for a_tokens in self.text_tokens:
            for i_index,s_ngram in enumerate(a_tokens):
                s_token = remove_all_punctuation(s_ngram).lower()
                if not s_token: s_token = s_ngram.strip()
                
                if i_index == 0:
                    ea_after[s_token].append('START')
                else:
                    ea_after[s_token].append(a_tokens[i_index-1])

                if i_index == len(a_tokens) - 1:
                    ea_before[s_token].append('END')
                else:
                    ea_before[s_token].append(a_tokens[i_index+1])

        ee_before_freq = {s_token:freq(a_tokens)
                          for s_token,a_tokens in ea_before.items()}
        ee_after_freq = {s_token:freq(a_tokens)
                         for s_token,a_tokens in ea_after.items()}

        self.token_befores = {s_token:freq_dict_to_percent(e_freq) for s_token,e_freq in ee_before_freq.items()}
        self.token_afters = {s_token:freq_dict_to_percent(e_freq) for s_token,e_freq in ee_after_freq.items()}
        
    
    def position_confidence(self, s_ngram, i_pos):
        ''' Score the relationships between the token and the before and after positions in both directions. '''
        s_token = remove_all_punctuation(s_ngram).lower()
        if not s_token: s_token = s_ngram.strip()
        
        e_element_post = self.token_befores.get(s_token,{})
            
        e_element_pre = self.token_afters.get(s_token,{})            

        d_before_score = 0
        if i_pos == 0:
            d_before_score = e_element_pre.get("START",0)
        else:
            s_pre = self.synth[i_pos-1]
            d_before_score += e_element_pre.get(s_pre,0)
            
            for s_pre in self.synth[:i_pos]:
                d_before_score += e_element_pre.get(s_pre,0)

        d_after_score = 0
        if i_pos >= len(self.synth):
            d_after_score = e_element_post.get("END",0.01)
        else:
            s_post = self.synth[i_pos]
            d_after_score += e_element_post.get(s_post,0)
            
            for s_post in self.synth[i_pos:]:
                d_after_score += e_element_post.get(s_post,0)

        if s_token in [r')',',','%']: return d_before_score
        elif s_token in [r'(','$']: return d_after_score
        else: return d_before_score + d_after_score
        
    
    def synthesize_pattern(self):
        ''' Create a pattern that contains all the tokens that appear in the texts.
            Add elements in by order of their length and by their commonality.
            For each token, look at the most frequent before and after.
            Try to find the best position relative to both. '''
        self.synth = []

        for t_ngram in reversed(self.common_tokens): #start with the ngrams in common between the texts
            s_token = ' '.join(t_ngram[1])
            d_best_score,i_best_pos = -1,0
            for i_pos in range(len(self.synth)+1):
                d_pos_score = self.position_confidence(s_token, i_pos)
                if d_pos_score > d_best_score:
                    i_best_pos = i_pos
                    d_best_score = d_pos_score
                    
            if d_best_score > 0: self.synth.insert(i_best_pos, s_token)
            else: self.synth.append(s_token)
                
        for t_ngram in reversed(self.uncommon_tokens): #then add the ngrams not in common between the texts
            s_token = ' '.join(t_ngram[1])
            d_best_score,i_best_pos = -1,0
            for i_pos in range(len(self.synth)+1):
                d_pos_score = self.position_confidence(s_token, i_pos)
                if d_pos_score > d_best_score:
                    i_best_pos = i_pos
                    d_best_score = d_pos_score

            if d_best_score > 0: self.synth.insert(i_best_pos, s_token)
            else: self.synth.append(s_token)
            
#end TextAgglomerator class